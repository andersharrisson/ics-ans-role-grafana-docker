import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_container(host):
    with host.sudo():
        grafana = host.docker("grafana")
        assert grafana.is_running


def test_api(host):
    cmd = host.run('curl -k -f https://ics-ans-role-grafana-docker-default/api/health')
    assert cmd.rc == 0
